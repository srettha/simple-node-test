function plus(arg1, arg2) {
  const parsedArg1 = parseInt(arg1, 10) || 0;
  const parsedArg2 = parseInt(arg2, 10) || 0;

  return parsedArg1 + parsedArg2;
}

module.exports = {
  plus,
};
