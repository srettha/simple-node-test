const { plus } = require("./index");

describe("plus", () => {
  it("should return 0 if nothing is given", () => {
    const actual = plus();
    expect(actual).toEqual(0);
  });

  it("should return result if another one is not given", () => {
    const actual = plus(1);
    expect(actual).toEqual(1);
  });

  it("should cast result if given input is string", () => {
    const actual = plus("1", "1");
    expect(actual).toEqual(2);
  });

  it("should return result from given input", () => {
    const actual = plus(1, 1);
    expect(actual).toEqual(2);
  });
});
